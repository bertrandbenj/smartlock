pragma solidity >= 0.5.0 < 0.7.0;

contract SmartLock {

    event addressRegistered(address identity);
    event addressRevoked(address identity);

    mapping (address => bool) internal MemberAddresses ;

    address payable public owner;

    constructor() public {
        owner = msg.sender;
    }


    function grantAccessTo(address identity) public {
        require( owner == msg.sender  , "The user granting Access must be owner");
        MemberAddresses[identity] = true;
        emit addressRegistered(identity);
    }

    function revokeAccessTo(address identity) public {
        require( owner == msg.sender  , "The user granting Access must be owner");
        delete MemberAddresses[identity];
        emit addressRevoked(identity);
    }

    function hasAccess(address identity) public view returns (bool res){
        require(MemberAddresses[msg.sender] , "The user verifying Accesses must be in the access list");
        res = MemberAddresses[identity];
    }

    function chOwner(address payable newOwner) public {
        owner = newOwner;
    }

    function isOwner(address identity) public view returns (bool res){
        res = owner == identity;
    }

    function getOwner() public view returns (address payable){
        return owner;
    }

}

contract LockOffer {

    SmartLock public theLock;
    uint public theAmount;


    constructor(SmartLock lock, uint amount) public {
        require( lock.isOwner(msg.sender),"The seller must own the lock");
        theLock = lock;
        theAmount = amount;
    }


    function buyLock() public {

        theLock.getOwner().transfer(theAmount); // if transfer fail then the function should revert any changes

        theLock.chOwner(msg.sender); // if transfer works, change the ownership


    }
}
